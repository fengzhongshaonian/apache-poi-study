package com.example;

import com.example.annotation.ExcelColumnProperty;

// 直播间信息
public class LiveRoomInfo {

    // 直播标题
    @ExcelColumnProperty(value = "直播标题*", index = 1)
    private String title;

    // 主讲人
    @ExcelColumnProperty(value = "主讲人*（多位主讲人以顿号隔开）", index = 2)
    private String broadcaster;

    // 直播简介
    @ExcelColumnProperty(value = "直播简介*", index = 3)
    private String introduction;

    // 开播时间
    @ExcelColumnProperty(value = "预计开播时间*", index = 4)
    private String startTime;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBroadcaster() {
        return broadcaster;
    }

    public void setBroadcaster(String broadcaster) {
        this.broadcaster = broadcaster;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
}
