package com.example;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.example.util.ExcelColumnUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class XlsxReadTest {

    public String getExcelFilePath(){
        String path = getClass().getClassLoader().getResource("excel/for-read.xlsx").getPath();
        return path;
    }

    @Test
    public void testReadExcel() throws IOException {
        String excelFilePath = getExcelFilePath();
        try (InputStream in = new FileInputStream(excelFilePath)){
            // 根据输入流创建工作簿
            XSSFWorkbook workbook = new XSSFWorkbook(in);
            // 获取第一个工作表
            XSSFSheet sheet = workbook.getSheetAt(0);
            // 获取总行数
            int rowNums = sheet.getPhysicalNumberOfRows();
            System.out.println("总行数：" + rowNums);
            // 循环遍历每行的内容
            // 工作行
            Row row = null;
            // 工作单元格
            Cell cell = null;
            for (int i = 8; i < rowNums; i++) {
                if(i == 9) continue;
                // sheet.getRow(i)用来获取第i行的工作行，第0行是表头
                row = sheet.getRow(i);
                // 当前行的总列数
                int colNums = row.getPhysicalNumberOfCells();
                System.out.println("第" + (i+1) + "行有" + colNums + "列");
                // 获取每个单元格的值
                for (int j = 1; j <= 4; j++) {
                    cell = row.getCell(j);
                    System.out.print(cell.toString() + "\t");
                }
                System.out.println();
            }

        }

    }


    @Test
    public void testReadExcelToEntity() throws IOException {
        List<LiveRoomInfo> liveRoomInfoList = new ArrayList<>();
        String excelFilePath = getExcelFilePath();
        try (InputStream in = new FileInputStream(excelFilePath)){
            // 根据输入流创建工作簿
            XSSFWorkbook workbook = new XSSFWorkbook(in);
            // 获取第一个工作表
            XSSFSheet sheet = workbook.getSheetAt(0);
            // 获取总行数
            int rowNums = sheet.getPhysicalNumberOfRows();
            System.out.println("总行数：" + rowNums);
            // 循环遍历每行的内容
            // 工作行
            Row row = null;
            // 工作单元格
            Cell cell = null;
            List<Field> fields = ExcelColumnUtil.listFields(LiveRoomInfo.class);
            for (int i = 10; i < rowNums; i++) {
                row = sheet.getRow(i);
                LiveRoomInfo liveRoomInfo = new LiveRoomInfo();
                for (Field field : fields){
                    int index = ExcelColumnUtil.getColumnIndex(field);
                    cell = row.getCell(index);
                    String value = cell.getStringCellValue();
                    ExcelColumnUtil.setValue(liveRoomInfo, field, value);
                }
                liveRoomInfoList.add(liveRoomInfo);
            }

            List<JSONObject> collect = liveRoomInfoList.stream().map(o -> (JSONObject)JSONObject.toJSON(o)).collect(Collectors.toList());
            collect.forEach(o -> {
                System.out.println(o.toString(SerializerFeature.PrettyFormat));
            });
        }
    }
}
